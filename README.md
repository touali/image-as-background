# Image as Background (WIP)

Image as Background is a Drupal 8 module that allow you to render your images as div background instead of html img element.

## Usage

Install and enable this module the way you like.
Create an image field or use an existing one and choose "Background" fied formatter.
This module use your defined image style so don't forget to use one.

### Composer install

Past this in your composer.json repositories 
    
    {
        "type": "vcs",
        "url": "https://bitbucket.org/touali/image-as-background"
    }


And then
    
    composer require touali/image_as_background

## TODO

* Code cleanup & improvement
* Customizable css class
* Composer dependencies